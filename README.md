IMPORTER FOR REUTERS DATASET INTO SOLR
======================================

Steps to use project:

+  Download the [dataset](http://www.daviddlewis.com/resources/testcollections/reuters21578/reuters21578.tar.gz) and untar it somewhere on your computer
+  Clone this repo and and set it up using ```mvn eclipse:eclipse``` and ```mvn install```
+  Open the file ```/ReutersSolrImporter/src/main/resources/importer.properties``` and update the properties to match the path to your dataset, the url to your solr instance and update the collection if you are using something other than "collection1"
+  Update solars schema.xml file to include the required fields (the schema I used is  in the root of the project, but is a work in progress)
+  Get your solr instance up and running
+  Run the Main java class
+  Sit back and enjoy watching the indexing of documents =)
+  Report bugs =(