package no.comperio.importer.solr.reuters.model;

public enum CGISplit {
	TRAINING_SET, PUBLISHED_TESTSET
}
