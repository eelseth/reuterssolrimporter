package no.comperio.importer.solr.reuters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import no.comperio.importer.solr.reuters.connectors.SolrConnector;
import no.comperio.importer.solr.reuters.model.Document;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

/**
 * Solr feeder will take a single, or a list of {@link Document}s and feed them to a Solr instance
 * @author eelseth
 */
public class SolrFeeder {
	SolrConnector connector;
	
	public SolrFeeder() throws IOException {
		connector = new SolrConnector();
	}
	private static final Logger LOGGER = Logger.getLogger(SolrFeeder.class);
	/**
	 * Takes a {@link List} of {@link Document}s and feeds them to a Solr instance 
	 * @param documents
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public void feed(List<Document> documents) throws ClientProtocolException, IOException {
		for (Document document : documents) {
			feed(document);
		}
	}
	
	/**
	 * Takes a {@link Document} and feeds it to a Solr instance
	 * @param document
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	
	public void feed(Document document) throws ClientProtocolException, IOException {
		connector.update(document);
		
	}
}

