package no.comperio.importer.solr.reuters.model;

public enum LewisSplit {
	TRAINING, TEST, NOT_USED
}
