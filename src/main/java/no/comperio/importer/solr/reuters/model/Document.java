package no.comperio.importer.solr.reuters.model;

import java.util.Date;
import java.util.List;

public class Document {
	private int oldId;
	private int id;
	private CGISplit cgiSplit;
	private LewisSplit lewisSplit;
	private Topics hadTopics;
	private Date date;
	List<String> mkNote;
	List<String> topics;
	List<String> places;
	List<String> people;
	List<String> organizations;
	List<String> exchanges;
	List<String> companies;
	String unknown;
	private Type textType;
	private String author;
	private String dateline;
	private String title;
	private String body;
	private String uri;
	
	public String getUri() {
		return uri;
	}

	/**
	 * Returns the date the article was reported
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Sets the date the article was reported
	 * @param date
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * Returns notes on certain hand corrections that were done to the original Reuters corpus by Steve Finch
	 * @return the notes
	 */
	public List<String> getMkNote() {
		return mkNote;
	}
	
	/**
	 * Sets the notes on certain hand corrections that were done to the original Reuters corpus by Steve Finch
	 * @param mkNote
	 */
	public void setMkNote(List<String> mkNote) {
		this.mkNote = mkNote;
	}
	
	/**
	 * Returns the list of topics categories, if any, for the document
	 * @return the topics
	 */
	public List<String> getTopics() {
		return topics;
	}
	
	/**
	 * Sets the list of topics categories, if any, for the document
	 * @param topics
	 */
	public void setTopics(List<String> topics) {
		this.topics = topics;
	}
	
	/**
	 * Returns the list of places categories, if any, for the document
	 * @return
	 */
	public List<String> getPlaces() {
		return places;
	}
	
	/**
	 * Sets the list of places categories, if any, for the document
	 * @param places
	 */
	public void setPlaces(List<String> places) {
		this.places = places;
	}
	
	
	/**
	 * Returns the list of people categories, if any, for the document
	 * @return
	 */
	public List<String> getPeople() {
		return people;
	}
	/**
	 * Sets the list of people categories, if any, for the document
	 * @param people
	 */
	public void setPeople(List<String> people) {
		this.people = people;
	}
	
	/**
	 * Returns the list of organization categories, if any, for the document
	 * @return
	 */
	public List<String> getOrganizations() {
		return organizations;
	}
	
	/**
	 * Sets the list of organization categories, if any, for the document
	 * @param organizations
	 */
	public void setOrganizations(List<String> organizations) {
		this.organizations = organizations;
	}
	
	/**
	 * Returns the list of exchange categories, if any, for the document
	 * @return
	 */
	public List<String> getExchanges() {
		return exchanges;
	}
	
	/**
	 * Sets the list of exchange categories, if any, for the document
	 * @param exchanges
	 */
	public void setExchanges(List<String> exchanges) {
		this.exchanges = exchanges;
	}
	
	/**
	 * Returns the list of company categories, if any, for the document
	 * @return
	 */
	public List<String> getCompanies() {
		return companies;
	}
	
	/**
	 * Sets the list of company categories, if any, for the document
	 * @param companies
	 */
	public void setCompanies(List<String> companies) {
		this.companies = companies;
	}
	
	/**
	 * Returns control characters and other noisy and/or somewhat mysterious material in the Reuters stories
	 * @return the unknown
	 */
	public String getUnknown() {
		return unknown;
	}
	/**
	 * Sets control characters and other noisy and/or somewhat mysterious material in the Reuters stories
	 * @param string
	 */
	public void setUnknown(String string) {
		this.unknown = string;
	}
	
	/**
	 * Returns the identification number (ID) the story had in the Reuters-22173 collection.
	 * @return the old id
	 */
	public int getOldId() {
		return oldId;
	}
	
	/**
	 * Sets the identification number (ID) the story had in the Reuters-22173 collection.
	 * @param the old id
	 */
	public void setOldId(int oldId) {
		this.oldId = oldId;
	}
	
	/**
	 * Returns the identification number (ID) the story has in the Reuters-21578, Distribution 1.0 collection.  These IDs are assigned to the stories in chronological order.
	 * @return the new id
	 */
	public int getNewId() {
		return id;
	}
	
	/**
	 * 	Sets the identification number (ID) the story has in the Reuters-21578, Distribution 1.0 collection.  These IDs are assigned to the stories in chronological order.
	 * @param the new id
	 */
	public void setNewId(int newId) {
		this.id = newId;
	}
	
	/**
	 * Returns an enum indicating whether the document was in the training set or the test set for the experiments reported in HAYES89 and HAYES90b.
	 * @return the enum set
	 */
	public CGISplit getCgiSplit() {
		return cgiSplit;
	}
	/**
	 * Sets an enum indicating whether the document was in the training set or the test set for the experiments reported in HAYES89 and HAYES90b.
	 * @param cgiSplit
	 */
	public void setCgiSplit(CGISplit cgiSplit) {
		this.cgiSplit = cgiSplit;
	}
	
	/**
	 * Returns an enum indicating if the article was part of the TRAINING or TEST sets of the experiments LEWIS91d (Chapters 9 and 10), LEWIS92b, LEWIS92e, and LEWIS94b
	 * @return The set enum
	 */
	public LewisSplit getLewisSplit() {
		return lewisSplit;
	}
	
	/**
	 * Setsan enum indicating if the article was part of the TRAINING or TEST sets of the experiments LEWIS91d (Chapters 9 and 10), LEWIS92b, LEWIS92e, and LEWIS94b
	 * @param the enum to set
	 */
	public void setLewisSplit(LewisSplit lewisSplit) {
		this.lewisSplit = lewisSplit;
	}
	
	/**
	 * Returns an enum indicating if the article had a topic in the original dataset.
	 * This poorly-named attribute unfortunately is the subject of much confusion. 
	 * It is meant to indicate whether or not the document had
	 * TOPICS categories *in the raw Reuters-22173 dataset*.  
	 * @return An enum indicating if the article had a topic in the original dataset
	 */
	public Topics getHadTopics() {
		return hadTopics;
	}
	
	/**
	 * Set an enum indicating if the article had a topic in the original reuters dataset.
	 * @param topics
	 */
	public void setHadTopics(Topics topics) {
		this.hadTopics = topics;
	}
	
	/**
	 * Returns the type of story. Either NORM  if he text of the story had a normal structure,
	 * BRIEF when the story is a short one or two line note,
	 * UNPROC when the format of the story is unusual in some fashion that limited the
	 * ability to further structure it
	 * @return the type
	 */
	public Type getTextType() {
		return textType;
	}
	
	/**
	 * Sets the type of story. Either NORM  if he text of the story had a normal structure,
	 * BRIEF when the story is a short one or two line note,
	 * UNPROC when the format of the story is unusual in some fashion that limited the
	 * ability to further structure it
	 * @return the type
	 */
	public void setTextType(Type type) {
		this.textType = type;
	}
	
	/**
	 * Returns the author of the article
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * Set the author of the article
	 * @param author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	/**
	 * Returns the location the story originated from, and day of the year. 
	 * @return the dateline
	 */
	public String getDateline() {
		return dateline;
	}
	
	/**
	 * Set the location the story originated from, and day of the year.
	 * @param dateline
	 */
	public void setDateline(String dateline) {
		this.dateline = dateline;
	}
	
	/**
	 * Returns the title of the article. If the type of the article is {@link Type.BRIEF} this might be the same as the body 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title of the article. If the type of the article is {@link Type.BRIEF} this might be the same as the body 
	 * @param the title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Returns the main body of the article, if the type of the article is {@link Type.BRIEF} this should be set to the title of the article
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	/**
	 * Sets the main body of the article, if the type of the article is {@link Type.BRIEF} this should be set to the title of the article
	 * @param the body
	 */
	public void setBody(String body) {
		this.body = body;
	}

	public void setUri(String file) {
		this.uri = file;
		
	}

}
