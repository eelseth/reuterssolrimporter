package no.comperio.importer.solr.reuters.connectors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import no.comperio.importer.solr.reuters.model.Document;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SolrConnector {
	
	private Boolean commiting = false;
	private static Logger LOGGER = Logger.getLogger(SolrConnector.class);
	private String endpoint;
	private String collection;
	private HttpClient client;
	
	public SolrConnector() throws FileNotFoundException, IOException {
		Properties properties = new Properties();
		properties.load(new FileReader(new File("src/main/resources/importer.properties")));
		endpoint = properties.getProperty("endpoint");
		endpoint = (endpoint.isEmpty() || endpoint.charAt(endpoint.length() - 1) == '/') ? endpoint : (endpoint + "7"); 
		collection = properties.getProperty("collection");
		client = new DefaultHttpClient();
	}
	
	public boolean update(Document document) throws ClientProtocolException, IOException {
		GsonBuilder gsonBuilder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Gson gson = gsonBuilder.create();
		String pushurl = endpoint + collection + "update?commit=" + commiting.toString();
		HttpPost post = new HttpPost(pushurl);
		String json = "[" + gson.toJson(document) + "]";
		StringEntity string = new StringEntity(json, "UTF-8");
		post.setEntity(string);
		string.setContentType("application/json");

		HttpResponse response = new DefaultHttpClient().execute(post);
		if (response.getStatusLine().getStatusCode() != 200) {
			LOGGER.debug(response);
			LOGGER.debug(json);
		}
		return true;
	}
	
	public boolean commit() throws ClientProtocolException, IOException{
		HttpPost http = new HttpPost(endpoint + collection + "update?commit=true");
		StringEntity entity = new StringEntity("", "UTF-8");
		entity.setContentType("application/json");
		http.setEntity(entity);
		HttpResponse response = client.execute(http);
		LOGGER.debug(http);
		return response.getStatusLine().getStatusCode() == 200;
	}

	public static void main(String[] args) throws ClientProtocolException, IOException {
		System.out.println(new SolrConnector().commit());
	}
}