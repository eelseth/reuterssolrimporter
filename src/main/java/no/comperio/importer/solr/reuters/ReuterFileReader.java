package no.comperio.importer.solr.reuters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import no.comperio.importer.solr.reuters.model.CGISplit;
import no.comperio.importer.solr.reuters.model.Document;
import no.comperio.importer.solr.reuters.model.LewisSplit;
import no.comperio.importer.solr.reuters.model.Topics;
import no.comperio.importer.solr.reuters.model.Type;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.xml.sax.SAXException;

public class ReuterFileReader {
	protected File file;
	private static final Logger LOGGER = Logger.getLogger(ReuterFileReader.class);
	protected static final String DELIMITER_START = "<D>";
	protected static final String DELIMITER_END = "</D>";
	private static final int TOPICS = 0;
	private static final int LEWIS = 1;
	private static final int CGI = 2;
	private static final int OLD_ID = 3;
	private static final int NEW_ID = 4;
	protected DocumentFactory documentFactory = new DocumentFactory();
	
	/**
	 * Returns the {@link Document}s that appear in the file 
	 * @return the documents
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public List<Document> getDocuments() throws IOException, ParseException {
		LOGGER.debug("Getting documents for file:" + getFile());
		BufferedReader reader = new BufferedReader(new FileReader(getFile()));
		List<Document> documents = new ArrayList<>();
		while (reader.ready()) {
			String line = reader.readLine();
			if (line.startsWith("<REUTERS ")) {
				Document document = buildDocument(line, reader);
				document.setUri("file://" + getFile().getAbsolutePath());
				documents.add(document);
			}
		}
		reader.close();
		return documents;
	}
	
	
	private Document buildDocument(String line, BufferedReader reader) throws IOException, ParseException {
		Document document = parseAttributes(line);
		parseElements(document, reader);
		return document;
	}


	private void parseElements(Document document, BufferedReader reader) throws IOException, ParseException {
		while ( reader.ready() ) {
			String line = reader.readLine();
			if (line.startsWith("<DATE>")) {
				document.setDate(parseDate(line));
			} else if (line.startsWith("<TOPICS>")) {
				document.setTopics(parseList(line));
			} else if (line.startsWith("<PLACES>")) {
				document.setPlaces(parseList(line));
			} else if (line.startsWith("<PEOPLE>")) {
				document.setPeople(parseList(line));
			} else if (line.startsWith("<ORGS>")) {
				document.setOrganizations(parseList(line));
			} else if (line.startsWith("<EXCHANGES>")) {
				document.setExchanges(parseList(line));
			} else if (line.startsWith("<COMPANIES>")) {
				document.setOrganizations(parseList(line));
			} else if (line.startsWith("<UNKNOWN>")) {
				document.setUnknown(parseUnknown(line, reader));
			} else if (line.startsWith("<TEXT")) {
				parseText(line, reader, document);
			} else if (line.startsWith("</REUTERS")) {
				break;
			}
		}
	}

	private void parseText(String titleLine, BufferedReader reader, Document document) throws IOException {
		document.setTextType(parseType(titleLine));
		
		if (document.getTextType().equals(Type.NORM)){
			parseNormalText(document, reader);
		} else if (document.getTextType().equals(Type.BRIEF)) {
			parseBriefText(document, reader);
		} else {
			parseUnprocText(document, reader);
		}
	}

	private void parseUnprocText(Document document, BufferedReader reader) throws IOException {
		StringBuilder sb = new StringBuilder();
		document.setAuthor("Unknown");
		String line = reader.readLine();
		document.setTitle(line);
		while (reader.ready()) {
			if (line.contains("</TEXT>")) {
				sb.append(line.replaceAll("</TEXT>", ""));
				break;
			}
			sb.append(line);
			line = reader.readLine();
		}
		document.setBody(sb.toString());
		
	}


	private void parseBriefText(Document document, BufferedReader reader) throws IOException  {
		document.setAuthor("Unknown");
		String line;
		while (reader.ready()) {
			line = reader.readLine();
			if ( line.contains("</TEXT>")) {
				break;
			}
			if (line.contains("<TITLE>")) {
				String titleAndBody = parseTitleAndBody(line);
				document.setBody(titleAndBody);
				document.setTitle(titleAndBody);
			}
			
		}
		
	}


	private String parseTitleAndBody(String line) {
		int firstTagEnd = line.indexOf(">");
		return line.substring(firstTagEnd + 1);
	}


	private void parseNormalText(Document document, BufferedReader reader) throws IOException {
		String line;
		while (reader.ready()) {
			line = reader.readLine();
			if (line.contains("<TITLE>")){
				document.setTitle(parseTitle(line));
			}
			if (line.contains("<AUTHOR>")) {
				document.setAuthor(parseAuthor(line));
			}
			if (line.contains("<DATELINE>")) {
				document.setDateline(parseDateLine(line));
			}
			if (line.contains("<BODY>")) {
				document.setBody(parseBody(line, reader));
				break;
			}
		}
	}


	private String parseAuthor(String line) {
		line = stripTags(line);
		line = line.replaceFirst("[\\W]+By\\W+", "");
		line = line.replaceFirst(", Reuters\\s*$", "");
		return line;
	}


	private String parseBody(String line, BufferedReader reader) throws IOException {
		StringBuilder sb = new StringBuilder();
		String body = "<BODY>";
		sb.append(line.substring(line.indexOf(body) + body.length()));
		while (reader.ready()) {
			line = reader.readLine();
			if (line.contains("</BODY>")) {
				sb.append(line.substring(0, line.indexOf("</BODY>")));
				break;
			} else {				
				sb.append(line);
			}
		}
		return sb.toString();
	}


	private String parseDateLine(String line) {
		int firstTagEnd = line.indexOf(">");
		int lastTagStart = line.lastIndexOf("</DATELINE>");
		if (lastTagStart == -1) {
			lastTagStart = line.length();
		}
		return line.substring(firstTagEnd + 1, lastTagStart).trim();
	}


	private String parseTitle(String line) {
		int firstTagEnd = line.indexOf(">");
		int lastTagStart = line.lastIndexOf("<");
		return line.substring(firstTagEnd + 1, lastTagStart);
	}


	private Type parseType(String line) {
		int typeStart = line.indexOf("TYPE=\"");
		if (typeStart == -1) {
			return Type.NORM;
		}
		line = line.substring(typeStart + "TYPE=\"".length());
		if (line.startsWith("BRIEF")) {
			return Type.BRIEF;
		}
		return Type.UNPROC;
	}


	private String parseUnknown(String line, BufferedReader reader) throws IOException {
		StringBuilder unknown = new StringBuilder(line.substring("<UNKNOWN>".length() + 1));
		while (reader.ready()) {
			line = reader.readLine();
			if (line.contains("</UNKNOWN>")){
				line = line.replaceFirst("</UNKNOWN>", "");
				unknown.append(line); 
				break;
			}
			unknown.append(line);
		}
		return unknown.toString();
	}


	private List<String> parseList(String topics) {
		topics = stripTags(topics);
		return delimitedList(topics);
	}


	private List<String> delimitedList(String delimitedList) {
		if (delimitedList.equals("")) {
			return Collections.emptyList();
		}
		delimitedList = stripTags(delimitedList);
		String[] list = delimitedList.split(DELIMITER_END + DELIMITER_START);
		return Arrays.asList(list);
	}


	private Date parseDate(String dateString) throws ParseException {
		dateString = stripTags(dateString);
		DateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss.SS", Locale.UK);
		return df.parse(dateString);
	}


	private String stripTags(String tagLine) {
		int firstTagEnd = tagLine.indexOf(">");
		int lastTagStart= tagLine.lastIndexOf("<");
		return tagLine.substring(firstTagEnd + 1, lastTagStart);
	}


	private Document parseAttributes(String line) {
		Document document = new Document();
		String attrs = line.substring("<REUTERS ".length(), line.length() - 1);
		String[] attributesArray = attrs.split(" ");
		
		String hadTopics = getAttribute(attributesArray, TOPICS);
		document.setHadTopics(getHadTopics(hadTopics));
		
		String lewisSplit = getAttribute(attributesArray, LEWIS);
		document.setLewisSplit(getLewisSplit(lewisSplit));
		
		String cgiSplit = getAttribute(attributesArray, CGI);
		document.setCgiSplit(getCgiSplit(cgiSplit));
		
		String oldId = getAttribute(attributesArray, OLD_ID);
		document.setOldId(Integer.parseInt(oldId));
		
		String newID = getAttribute(attributesArray, NEW_ID);
		document.setNewId(Integer.parseInt(newID));

		return document;
	}


	private CGISplit getCgiSplit(String cgiSplit) {
		if (cgiSplit.equals("TRAINING-SET")) {
			return CGISplit.TRAINING_SET;
		}
		return CGISplit.PUBLISHED_TESTSET;
	}


	private LewisSplit getLewisSplit(String lewisSplit) {
		if (lewisSplit.startsWith("TRAIN")) {
			return LewisSplit.TRAINING;
		}
		if (lewisSplit.startsWith("TEST")) {
			return LewisSplit.TEST;
		}
		return LewisSplit.NOT_USED;
	}


	private Topics getHadTopics(String hadTopics) {
		if (hadTopics.equalsIgnoreCase("YES")) {
			return Topics.YES;
		}
		if (hadTopics.equalsIgnoreCase("NO")) {
			return Topics.NO;
		}
		return Topics.BYPASS;
	}


	private String getAttribute(String[] array, int attribute) {
		return array[attribute].substring(array[attribute].indexOf("\"") + 1, array[attribute].length()-1);
	}


	public static void main(String[] args) throws DocumentException, ParserConfigurationException, SAXException, IOException, ParseException {
		FileLocator locator = new FileLocator();
		List<File> files = locator.locateFiles();
		ReuterFileReader reader = new ReuterFileReader();
		for(File file : files) {
			reader.setFile(file);
			reader.getDocuments();
		}
	}

	/**
	 * Returns a reuters SGML file containing articles 
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Sets a reuters SGML file containing articles
	 * @param the file
	 */
	public void setFile(File file) {
		this.file = file;
	}
}