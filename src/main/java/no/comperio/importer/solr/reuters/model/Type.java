package no.comperio.importer.solr.reuters.model;

public enum Type {
	NORM, BRIEF, UNPROC
}
