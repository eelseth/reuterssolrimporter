package no.comperio.importer.solr.reuters;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

public class FileLocator 
{
    private static final Logger LOGGER = Logger.getLogger(FileLocator.class);
    private String baseFolder = "C:\\Users\\eelseth\\datasets\\reuters";
    private String fileType = "sgm";
    
	/**
	 * Returns a list of the files of the given type
	 * @return a List of files
	 */
	public List<File> locateFiles() {
		File folder = new File(baseFolder);
		if (!folder.isDirectory()) {
			throw new IllegalArgumentException("Path " + baseFolder + " does not point to a folder");
		}
		File[] fileArray = folder.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.getName().endsWith(fileType);
			}
		});
		List<File> files = Arrays.asList(fileArray);
		LOGGER.debug("Found " + files.size() + " files in folder");
		return files;
	}


	public String getBaseFolder() {
		return baseFolder;
	}


	public void setBaseFolder(String baseFolder) {
		this.baseFolder = baseFolder;
	}


	public String getFileType() {
		return fileType;
	}


	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
