package no.comperio.importer.solr.reuters;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import no.comperio.importer.solr.reuters.connectors.SolrConnector;

public class Main {
	private static final Logger LOGGER = Logger.getLogger(Main.class);
	
	public static void main(String[] args) throws IOException, ParseException {
		Properties properties = new Properties();
		properties.load(new FileReader(new File("src/main/resources/importer.properties")));
		String pathToDataSet = properties.getProperty("folder");

		ReuterFileReader fileReader = new ReuterFileReader();
		FileLocator fileLocator = new FileLocator();
		fileLocator. setBaseFolder(pathToDataSet);
		LOGGER.debug("Locating files");
		List<File> files = fileLocator.locateFiles();
		LOGGER.debug("Files located, index document");
		long timeAtStart = System.currentTimeMillis();
		for (File file : files) {
			fileReader.setFile(file);
			new SolrFeeder().feed(fileReader.getDocuments());
			LOGGER.debug("All articles from file indexed");
		}
		long timeAtEnd = System.currentTimeMillis();
		LOGGER.debug("Indexing of all files completed in " + ((timeAtEnd - timeAtStart) / 1000.0) + " seconds");
		new SolrConnector().commit();
	}
}

